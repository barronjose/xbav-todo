# xbAV - Coding challenge
This coding challenge to create a TODO application using Vue and Node.js

## Dependencies
- Node
- Homebrew
- YARN

## Install Node
To see Node documentation of how to install it click [here](https://nodejs.org/en/)

## Install Homebrew
To see Homebrew documentation of how to install it click [here](https://brew.sh/)

## Install yarn
Once homebrew is installed use it to install yarn
```
brew install yarn
```

## Install Library dependencies
To install library dependencies run the following command on the root folder of bot projects todo-api and todo-app
```
yarn install
```
# APP - setup
Run ```cd todo-app``` and then run ```yarn install```

## Start Application Project
Run ```yarn serve``` to start the project, it would be available on ```http://localhost:8080/```

## Start storybook
The project includes storybook and can de accessed via ```yarn storybook:serve``` it would be available on ```http://localhost:6006/```

## Lint
To improve code quality the project includes lint using Airbnb recommended configurarion and can be run with the following command
```
yarn lint
``` 

# API - setup
Run ```cd todo-api``` and then run ```yarn install```

## Start API - Project
Run ``` yarn start ``` to get the API up and running

## Testing
NOTE: You need to kill the server
Run: ```yarn test``` to get the feedback from the API testing

## Available API endpoints

GET ```http://localhost:5000/api/v1/todos``` - to fetch all the todos  
POST ```http://localhost:5000/api/v1/todos``` - create a newly todo (You need to provide a title, description, date and optional a completedAt)  
PUT ```http://localhost:5000/api/v1/todos/:id``` - Update a created todo, you need to provide the todo id  
DELETE ```http://localhost:5000/api/v1/todos/:id``` - Delete todo, you need to provide the todo id  