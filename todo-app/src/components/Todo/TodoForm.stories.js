import { storiesOf } from '@storybook/vue';

import TodoForm from './TodoForm.vue';

storiesOf('TodoForm', module).add('with text', () => ({
  components: { TodoForm },
  template: '<todo-form :todo="todo" />',
}));
