/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue';
// import { action } from '@storybook/addon-actions';
// import { linkTo } from '@storybook/addon-links';

import TodoItem from './Item.vue';

storiesOf('Item', module).add('with text', () => ({
  components: { TodoItem },
  data() {
    return {
      todo: {
        done: false,
        title: 'This is a todo',
      },
    };
  },
  template: '<todo-item :todo="todo" />',
  // methods: { action: action('clicked') },
}));
// .add('with JSX', () => ({
//   components: { MyButton },
//   render() {
//     return <my-button onClick={this.action}>With JSX</my-button>;
//   },
//   methods: { action: linkTo('Button', 'with some emoji') },
// }))
// .add('with some emoji', () => ({
//   components: { MyButton },
//   template: '<my-button @click="action">😀 😎 👍 💯</my-button>',
//   methods: { action: action('clicked') },
// }));
