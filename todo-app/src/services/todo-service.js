import api from '@/services/config-service.js';

export default {
  getTodos() {
    return api.get('/todos');
  },
  createTodo(todo) {
    return api.post('/todos', todo);
  },
  updateTodo(todo) {
    return api.put(`/todos/${todo.id}`, todo);
  },
  deleteTodo(id) {
    return api.delete(`/todos/${id}`);
  },
};
