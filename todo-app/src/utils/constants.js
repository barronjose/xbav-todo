export const EMPTY_TODO = {
  title: null,
  description: null,
  date: new Date().toISOString().substr(0, 10),
  completedAt: null,
};
