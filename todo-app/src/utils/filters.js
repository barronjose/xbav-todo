import Vue from 'vue';

Vue.filter('dateParser', value => {
  return new Date(value).toISOString().substr(0, 10);
});
