import TodoService from '@/services/todo-service.js';

export const namespaced = true;
export const state = {
  todos: [],
  todo: {},
  showDialog: false,
};

export const mutations = {
  SET_TODOS(state, todos) {
    state.todos = todos;
  },
  CREATE_TODO(state, todo) {
    state.todos.push(todo);
  },
  SET_TODO(state, todo) {
    state.todo = todo;
  },
  DELETE_TODO(state, id) {
    state.todos = state.todos.filter(todo => todo.id !== id);
  },
  UPDATE_TODO(state, todo) {
    const index = state.todos.findIndex(
      statedTodos => statedTodos.id === todo.id
    );
    state.todos = [
      ...state.todos.slice(0, index),
      todo,
      ...state.todos.slice(index + 1, state.todos.length),
    ];
  },
  SET_DISPLAY_DIALOG(state, show) {
    state.showDialog = show;
  },
};

export const actions = {
  fetchTodos({ commit }) {
    return TodoService.getTodos().then(response => {
      commit('SET_TODOS', response.data);
      return response;
    });
  },
  createTodo({ commit }, todo) {
    return TodoService.createTodo(todo).then(response => {
      commit('CREATE_TODO', response.data);
      return response;
    });
  },
  updateTodo({ commit }, todo) {
    return TodoService.updateTodo(todo).then(response => {
      commit('UPDATE_TODO', response.data);
      return response;
    });
  },
  deleteTodo({ commit }, id) {
    return TodoService.deleteTodo(id).then(() => {
      commit('DELETE_TODO', id);
      return;
    });
  },
  setTodo({ commit }, todo) {
    commit('SET_TODO', todo);
  },
  showDialog({ commit }, show) {
    commit('SET_DISPLAY_DIALOG', show);
  },
};

export const getters = {
  todos(state) {
    return state.todos;
  },
};
