module.exports = {
  tabWidth: 2,
  singleQuote: true,
  proseWrap: 'always',
  bracketSpacing: true,
  trailingComma: 'es5',
};
