import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import shortid from 'shortid';

const adapter = new FileSync('./data/db.json');
const db = low(adapter);
db.defaults({ todos: [] });

class TodoController {
  static getTodos(req, res) {
    const todos = db.get('todos');
    return res.status(200).json(todos);
  }

  static getTodo(req, res) {
    const todo = db.get('todos').find({ id: req.params.id });
    if (todo) {
      return res.status(200).json(todo);
    }
    return res.status(404).json({
      message: 'Todo not found',
    });
  }

  static createTodo(req, res) {
    const actualDate = new Date();
    const todo = {
      ...req.body,
      id: shortid.generate(),
      createdAt: actualDate,
      completedAt: req.body.completedAt ? actualDate : null,
      updatedAt: null,
    };
    db.get('todos')
      .push(todo)
      .write();
    if (todo) {
      return res.status(200).json(todo);
    }
    return res.status(404).json({
      message: 'Todo not found',
    });
  }

  static updateTodo(req, res) {
    const actualDate = new Date();
    const todo = {
      ...req.body,
      completedAt: req.body.completedAt ? actualDate : null,
      updatedAt: actualDate,
    };
    const updatedTodo = db.get('todos')
      .find({ id: req.params.id })
      .assign(todo)
      .write();
   if (updatedTodo) {
      return res.status(200).json(updatedTodo);
    }
    return res.status(404).json({
      message: 'Todo not found',
    });
  }

  static deleteTodo(req, res) {
    db.get('todos')
      .remove({ id: req.params.id })
      .write();
    return res.status(200).json({});
  }
}

export default TodoController;
