import express from 'express';
import todosRoutes from './todos-routes';

const router = express.Router();

router.use('/todos', todosRoutes);

export default router;
