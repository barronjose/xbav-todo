import express from 'express';
import TodoController from '../../controllers/todo-controller';

const routes = express.Router();

routes.get('/', TodoController.getTodos);
routes.get('/:id', TodoController.getTodo);
routes.post('/', TodoController.createTodo);
routes.put('/:id', TodoController.updateTodo);
routes.delete('/:id', TodoController.deleteTodo);

export default routes;
