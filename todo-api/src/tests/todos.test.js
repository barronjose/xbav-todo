import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server';

const API_VERSION = '/api/v1/';

chai.use(chaiHttp);
chai.should();

let testTodo = {};

describe('Todos', () => {
  describe('GET /', () => {
    it('should return all todos in json file', done => {
      chai
        .request(app)
        .get(`${API_VERSION}todos`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
  });

  describe('POST /', () => {
    const post = {
      title: 'Some test title',
      description: 'Description for the test todo',
      date: new Date().toISOString().substr(0, 10),
      completedAt: null,
    };
    it('should create a new todo and return it', done => {
      chai
        .request(app)
        .post(`${API_VERSION}todos`)
        .end((err, res) => {
          testTodo = res.body;
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });

  describe('PUT /', () => {
    it('should update the previously created todo', done => {
      chai
        .request(app)
        .put(`${API_VERSION}todos/${testTodo.id}`, testTodo)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });

  describe('DELETE /', () => {
    it('should delete the previously updated todo', done => {
      chai
        .request(app)
        .delete(`${API_VERSION}todos/${testTodo.id}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });
});
